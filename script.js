const numInput = document.getElementById("num");
const outputDiv = document.getElementById("output");
const submitButton = document.getElementById("rel");
const inputDiv = document.getElementById("input-div");

let generated = false
// Clear previous output
function generateFibonacci() {
    let a = 0, b = 1;
    
    let arr = []
    outputDiv.innerHTML = "";
    
    // Get the number of Fibonacci numbers to generate
    const num = parseInt(numInput.value);
    
    // Check for valid input
    if (isNaN(num) || num < 1) {
        outputDiv.innerHTML = "Please enter a valid positive number.";
        return;
    }
    outputDiv.style.visibility = 'visible';

    inputDiv.style.visibility = 'hidden';
    // Generate the Fibonacci series
     
    for (let i = 0; i < num; i++) {
        arr.push(a)
        const temp = a + b;
        a = b;
        b = temp;
    }
    outputDiv.innerHTML = arr.join(" ")
    generated = true
    
}

function showInput() {
    inputDiv.style.visibility = 'visible'
    outputDiv.style.visibility = 'hidden'
}